// calendars module router
// author: Ahmet Tashan
// date: 12.05.2023 08:16:23

export default {
    path: '/calendar',
    name: 'calendar',
    component: () => import('./Module.vue'),
    children: [
        {
            path: '',
            name: 'calendar:index',
            component: () => import('./views/CalendarIndexView.vue'),
            meta: {
                template: 'default', // default, admin, blank
            }
        },
    ],
}
