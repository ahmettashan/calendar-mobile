// calendars module options
// author: Ahmet Tashan
// date: 12.05.2023 08:16:23

import store from './store';
import routes from './router.calendar';

export default {
    name: 'calendar',
    store,
    routes,
}
