const fs = require('fs');
const pluralize = require('pluralize');

class Modules {

    // node module argument
    argument;

    filename = {singular: '', plural: '', pascalCase: ''}

    // module path
    modulePath = 'src/modules/';

    constructor() {
        this.argument = process.argv[2];

        if (!this.argument) {
            this.alert(['Lütfen bir argüman giriniz.', 'Örnek: node module argument']);
            return;
        }

        this.init();
    }

    // --------------------------------------------

    async init() {
        // isimlerin çoğul ve tekil hallerini al
        this.setFilename();

        // modules klasörünü oluştur
        try {
            await this.createModule();
        } catch (err) {
            return this.alert(['/src klasörünün altına /modules klasörü oluşturulamadı.', 'Manuel olarak oluşturun.'], 'warning');
        }

        if (fs.existsSync(this.modulePath + this.filename.plural)) {
            return this.alert(['Modül zaten mevcut.'], 'warning');
        }

        try {
            // module klasörünü oluştur
            await this.createFolder(`${this.modulePath}${this.filename.plural}/`);
            this.modulePath += this.filename.plural + '/';

            // module options dosyasını oluştur
            await this.createModuleOptions();

            // store dosyasını oluştur
            await this.createStore();

            // router dosyasını oluştur
            await this.createRouter();

            // vue dosyalarını oluştur
            await this.createVue();

            // component klasörü oluştur
            await this.createFolder(`${this.modulePath}components/`);

            // service klasörünü oluştur
            await this.createService();

            this.alert(['Modül oluşturuldu.', 'Modül adı: ' + this.filename.plural, 'Modül path: ' + this.modulePath], 'success');

        } catch (e) {

            fs.existsSync(this.modulePath) && fs.rmdirSync(this.modulePath, {recursive: true});

            this.alert(['Modül oluşturulamadı.', 'Hata: ' + e.message], 'error');

            return false;
        }


    }

    // --------------------------------------------

    // Create Module
    async createModule() {
        return this.createFolder('./src/modules');
    }

    // Create Module Options
    async createModuleOptions() {
        let filename = this.filename;
        let modulePath = this.modulePath;
        let content = `// ${filename.plural} module options
// author: Ahmet Tashan
// date: ${new Date().toLocaleString()}

import store from './store';
import routes from './router.${filename.singular}';

export default {
    name: '${filename.singular}',
    store,
    routes,
}
`;
        await this.createFile(`${modulePath}module.js`, content);
    }

    // Create Store
    async createStore() {
        let filename = this.filename;
        let modulePath = this.modulePath;

        await this.createFolder(`${modulePath}store/`);

        let content = `// ${filename.plural} module store
// author: Ahmet Tashan
// date: ${new Date().toLocaleString()}

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {},
}
`;
        await this.createFile(`${modulePath}store/index.js`, content);
    }

    // Create Router
    async createRouter() {
        let filename = this.filename;
        let modulePath = this.modulePath;
        let content = `// ${filename.plural} module router
// author: Ahmet Tashan
// date: ${new Date().toLocaleString()}

export default {
    path: '/${filename.singular}',
    name: '${filename.singular}',
    component: () => import('./Module.vue'),
    children: [
        {
            path: '',
            name: '${filename.singular}:index',
            component: () => import('./views/${filename.pascalCase}IndexView.vue'),
            meta: {
                template: 'default', // default, admin, blank
            }
        },
    ],
}
`;

        await this.createFile(`${modulePath}router.${filename.singular}.js`, content);
    }

    // Create Module.vue, View folder and IndexView.vue
    async createVue() {
        let filename = this.filename;
        let modulePath = this.modulePath;
        let content = `<template>
        <router-view/>
</template>`;
        await this.createFile(`${modulePath}Module.vue`, content);

        // views klasörünü oluştur
        await this.createFolder(`${modulePath}views/`);

        // index view dosyasını oluştur
        content = `<script>
    export default {
        name: '${filename.pascalCase}IndexView',
    }
</script>

<template>
    <div>
        <h1>${filename.pascalCase} Index View</h1>
    </div>
</template>

<style scoped>
</style>`;
        await this.createFile(`${modulePath}views/${filename.pascalCase}IndexView.vue`, content);
    }

    async createService() {
        let filename = this.filename;
        let modulePath = this.modulePath;

        await this.createFolder(`${modulePath}services/`);

        let content = `// ${filename.plural} service
// author: Ahmet Tashan
// date: ${new Date().toLocaleString()}`;

        await this.createFile(`${modulePath}services/${filename.singular}.service.js`, content);
    }

    // --------------------------------------------

    // Create Folder
    async createFolder(filename) {
        if (fs.existsSync(filename)) return true;

        try {
            await fs.promises.mkdir(filename);
            return true;
        } catch (err) {
            throw new err;
        }
    }

    // Create File
    async createFile(filename, content) {
        if (fs.existsSync(filename)) return true;

        try {
            await fs.promises.writeFile(filename, content);
            return true;
        } catch (err) {
            throw new err;
        }
    }

    // --------------------------------------------

    // Alert
    alert(message, color = 'red') {

        let consoleColor = '\x1b[31m';

        if (color === 'warning' || color === 'yellow') {
            consoleColor = '\x1b[33m';
        }

        if (color === 'success' || color === 'green') {
            consoleColor = '\x1b[32m';
        }

        if (typeof message === 'number') {
            message = message.toString();
        }

        if (typeof message === 'string') {
            message = '\n  - ' + message + '\n';
        }

        if (typeof message === 'object') {
            message = '\n  - ' + message.join('\n  - ') + '\n';
        }

        console.log(consoleColor + "%s\x1b[0m", message);
    }

    setFilename() {
        // filename
        let filename = this.argument;

        // filename properties düzenle
        this.filename.singular = pluralize.singular(filename);
        this.filename.plural = pluralize.plural(filename);

        // Özel karakterleri boşlukla değiştir
        filename = filename.replace(/[^a-zA-Z0-9 ]/g, ' ');
        // Boşlukları tek boşlukla değiştir
        filename = filename.replace(/\s\s+/g, ' ');

        // PascalCase
        let words = filename.split(' ');
        for (let i = 0; i < words.length; i++) {
            words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
        this.filename.pascalCase = words.join('');
    }

}

new Modules();
